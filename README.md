# autopandoc-GitLab

Ce projet est une adaptation à GitLab d'autopandoc d'<a href=https://github.com/infologie/autopandoc>autopandoc</a> d'Arthur Perret.
Il permet d'automatiser l'exécution de pandoc via les outils d'intégration continue de GitLab.